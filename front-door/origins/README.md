<!---------------------------------------------------------------------------->

# front-door/origins

#### [Azure] [Front Door] origin groups and their respective origins

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cdn/azure//front-door/origins`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = "uksouth"
}

module "my_front_door_profile" {
  source  = "gitlab.com/bitservices/cdn/azure//front-door/profile"
  name    = "foobar-frontdoor"
  group   = module.my_resource_group.name
  owner   = var.owner
  company = var.company
}

module "my_front_door_origins" {
  source         = "gitlab.com/bitservices/cdn/azure//front-door/origins"
  class          = null
  group          = module.my_resource_group.name
  profile_id     = module.my_front_door_profile.id
  profile_name   = module.my_front_door_profile.name
  profile_lookup = false

  origins = {
    "foo" = {
      endpoint = "origin.foo.bitservices.io"
    }
  }
}
```

<!---------------------------------------------------------------------------->

[Azure]:      https://azure.microsoft.com/
[Front Door]: https://azure.microsoft.com/services/frontdoor/

<!---------------------------------------------------------------------------->
