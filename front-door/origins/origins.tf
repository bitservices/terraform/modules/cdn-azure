###############################################################################
# Required Variables
###############################################################################

variable "origins" {
  type = map(object({
    weight       = optional(number)
    enabled      = optional(bool)
    endpoint     = string
    priority     = optional(number)
    http_port    = optional(number)
    https_port   = optional(number)
    tls_verify   = optional(bool)
    host_header  = optional(string)
    private_link = optional(object({
      message     = optional(string)
      location    = string
      target_id   = string
      target_type = string
    }))
  }))
  description = "A list of origins to configure for this Azure Front Door origin group."

  validation {
    condition     = length(var.origins) >= 1
    error_message = "There must be at least one origin configured for this Azure Front Door origin group."
  }

  validation {
    condition     = alltrue([ for origin in var.origins: origin.private_link == null ? true : contains([ "blob", "blob_secondary", "lb", "sites", "web" ], origin.private_link.target_type) ])
    error_message = "Target types in origin private links must be either: 'blob', 'blob_secondary', 'lb', 'sites' or 'web'."
  }
}

###############################################################################
# Optional Variables
###############################################################################

variable "origins_default_message" {
  type        = string
  default     = "[Terraform] Access request for Azure Front Door private link"
  description = "Specifies the request message that will be submitted to the private link target."
}

###############################################################################

variable "origins_http_default_port" {
  type        = number
  default     = 80
  description = "Default HTTP port for Azure Front Door origins."
}

variable "origins_https_default_port" {
  type        = number
  default     = 443
  description = "Default HTTPS port for Azure Front Door origins."
}

###############################################################################

variable "origins_tls_default_verify" {
  type        = bool
  default     = true
  description = "Should origin TLS certificates be verified by default."
}

###############################################################################
# Locals
###############################################################################

locals {
  origins = defaults(var.origins, {
    weight       = 500
    enabled      = true
    priority     = 3
    http_port    = var.origins_http_default_port
    https_port   = var.origins_https_default_port
    tls_verify   = var.origins_tls_default_verify
    private_link = {
      message = var.origins_default_message
    }
  })
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_cdn_frontdoor_origin" "object" {
  for_each                       = local.origins
  name                           = format("%s-%s", local.name, each.key)
  weight                         = each.value.weight
  enabled                        = each.value.enabled
  priority                       = each.value.priority
  host_name                      = each.value.endpoint
  http_port                      = each.value.http_port
  https_port                     = each.value.https_port
  origin_host_header             = coalesce(each.value.host_header, each.value.endpoint)
  cdn_frontdoor_origin_group_id  = azurerm_cdn_frontdoor_origin_group.object.id
  certificate_name_check_enabled = each.value.tls_verify

  dynamic "private_link" {
    for_each = each.value.private_link == null ? [] : [ each.value.private_link ]

    content {
      location               = each.value.location
      target_type            = each.value.target_type
      request_message        = each.value.message
      private_link_target_id = each.value.target_id
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "origins_default_message" {
  value = var.origins_default_message
}

###############################################################################

output "origins_http_default_port" {
  value = var.origins_http_default_port
}

output "origins_https_default_port" {
  value = var.origins_https_default_port
}

###############################################################################

output "origins_tls_default_verify" {
  value = var.origins_tls_default_verify
}

###############################################################################

output "origins" {
  value = { for key,value in azurerm_cdn_frontdoor_origin.object: key => merge(var.origins[key], { "id" = value.id, "name" = value.name }) }
}

output "origins_ids" {
  value = [ for value in azurerm_cdn_frontdoor_origin.object : value.id ]
}

###############################################################################
