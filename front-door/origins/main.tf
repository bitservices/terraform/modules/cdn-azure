###############################################################################
# Required Variables
###############################################################################

variable "class" {
  type        = string
  description = "The identifier of the Azure Front Door origin group within the Azure Front Door profile. Set to 'null' for no identifier (assume same name as the profile)."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

###############################################################################
# Optional Variables
###############################################################################

variable "delay" {
  type        = number
  default     = 2
  description = "Delay in minutes for routing traffic to new or restored endpoints."
}

variable "sticky_sessions" {
  type        = bool
  default     = true
  description = "Specifies whether session affinity should be enabled on this host."
}

###############################################################################

variable "health_path" {
  type        = string
  default     = "/"
  description = "Specifies the path relative to the origin that is used to determine the health of the origin."
}

variable "health_method" {
  type        = string
  default     = "HEAD"
  description = "The HTTP method to use for the health probe."
}

variable "health_interval" {
  type        = number
  default     = 30
  description = "Specifies the number of seconds between each health probe."
}

variable "health_protocol" {
  type        = string
  default     = "None"
  description = "Specifies the protocol to use for the health probe or 'None' to disable."

  validation {
    condition     = contains([ "Https", "Http", "None" ], var.health_protocol)
    error_message = "Health check protocol must be either: 'Https', 'Http' or 'None'."
  }
}

###############################################################################

variable "load_balancing_optimal_latency" {
  type        = number
  default     = 50
  description = "Specifies the latency in milliseconds for probes to fall into the lowest latency bucket in milliseconds."
}

variable "load_balancing_samples_decisions" {
  type        = number
  default     = 3
  description = "Specifies the number of samples to consider for load balancing decisions."
}

variable "load_balancing_samples_successful" {
  type        = number
  default     = 3
  description = "Specifies the number of samples within the sample period that must succeed."
}

###############################################################################
# Locals
###############################################################################

locals {
  name           = var.class == null ? var.profile_name : format("%s-%s", var.profile_name, var.class)
  health_enabled = var.health_protocol != "None"
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_cdn_frontdoor_origin_group" "object" {
  name                                                      = local.name
  cdn_frontdoor_profile_id                                  = local.profile_id
  session_affinity_enabled                                  = var.sticky_sessions
  restore_traffic_time_to_healed_or_new_endpoint_in_minutes = var.delay

  dynamic "health_probe" {
    for_each = local.health_enabled ? [ null ] : []

    content {
      path                = var.health_path
      protocol            = var.health_protocol
      request_type        = var.health_method
      interval_in_seconds = var.health_interval
    }
  }

  load_balancing {
    sample_size                        = var.load_balancing_samples_decisions
    successful_samples_required        = var.load_balancing_samples_successful
    additional_latency_in_milliseconds = var.load_balancing_optimal_latency
  }
}

###############################################################################
# Outputs
###############################################################################

output "class" {
  value = var.class
}

output "group" {
  value = var.group
}

###############################################################################

output "delay" {
  value = var.delay
}

output "sticky_sessions" {
  value = var.sticky_sessions
}

###############################################################################

output "health_path" {
  value = var.health_path
}

output "health_method" {
  value = var.health_method
}

output "health_enabled" {
  value = local.health_enabled
}

output "health_interval" {
  value = var.health_interval
}

output "health_protocol" {
  value = var.health_protocol
}

###############################################################################

output "load_balancing_optimal_latency" {
  value = var.load_balancing_optimal_latency
}

output "load_balancing_samples_decisions" {
  value = var.load_balancing_samples_decisions
}

output "load_balancing_samples_successful" {
  value = var.load_balancing_samples_successful
}

###############################################################################

output "id" {
  value = azurerm_cdn_frontdoor_origin_group.object.id
}

output "name" {
  value = azurerm_cdn_frontdoor_origin_group.object.name
}

###############################################################################
