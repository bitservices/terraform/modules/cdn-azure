###############################################################################
# Required Variables
###############################################################################

variable "profile_name" {
  type        = string
  description = "The full name of the Azure Front Door profile to associate this Azure Front Door custom domain with."
}

###############################################################################
# Optional Variables
###############################################################################

variable "profile_id" {
  type        = string
  default     = null
  description = "The ID of the Azure Front Door profile to associate this Azure Front Door custom domain with. Must be specified if the profile is created in the same Terraform run."
}

variable "profile_lookup" {
  type        = bool
  default     = true
  description = "Should the ID of the Azure Front Door profile be looked up from 'profile_name'. Must be 'true' if 'profile_id' is not specified."
}

###############################################################################
# Locals
###############################################################################

locals {
  profile_id = var.profile_lookup ? data.azurerm_cdn_frontdoor_profile.object[0].id : var.profile_id
}

###############################################################################
# Data Sources
###############################################################################

data "azurerm_cdn_frontdoor_profile" "object" {
  count               = var.profile_lookup ? 1 : 0
  name                = var.profile_name
  resource_group_name = var.group
}

###############################################################################
# Outputs
###############################################################################

output "profile_name" {
  value = var.profile_name
}

###############################################################################

output "profile_id" {
  value = local.profile_id
}

output "profile_lookup" {
  value = var.profile_lookup
}

###############################################################################
