###############################################################################
# Required Variables
###############################################################################

variable "class" {
  type        = string
  description = "The identifier of the Azure Front Door custom domain within the Azure Front Door profile."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

variable "domain" {
  type        = string
  description = "The fully qualified domain name for the Azure Front Door custom domain."
}

###############################################################################
# Optional Variables
###############################################################################

variable "tls_type" {
  type        = string
  default     = "ManagedCertificate"
  description = "Type of TLS configuration to use for this Azure Front Door custom domain. Must be 'CustomerCertificate' or 'ManagedCertificate'."

  validation {
    condition     = contains([ "CustomerCertificate", "ManagedCertificate" ], var.tls_type)
    error_message = "The TLS configuration type must be either 'CustomerCertificate' or 'ManagedCertificate'."
  }
}

variable "tls_secret" {
  type        = string
  default     = null
  description = "The resource ID containing the Azure Front Door custom domain TLS certificate. Ignored unless 'tls_type' is 'CustomerCertificate'."
}

variable "tls_release" {
  type        = string
  default     = "TLS12"
  description = "The minimum TLS version available on this Azure Front Door custom domain. Must be 'TLS10' or 'TLS12'."

  validation {
    condition     = contains([ "TLS10", "TLS12" ], var.tls_release)
    error_message = "The TLS release minimum version must be either 'TLS10' or 'TLS12'."
  }
}

###############################################################################
# Locals
###############################################################################

locals {
  name       = format("%s-%s", var.profile_name, var.class)
  tls_secret = var.tls_type == "CustomerCertificate" ? var.tls_secret : null
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_cdn_frontdoor_custom_domain" "object" {
  name                     = local.name
  host_name                = var.domain
  dns_zone_id              = local.dns_zone_id
  cdn_frontdoor_profile_id = local.profile_id

  tls {
    certificate_type        = var.tls_type
    minimum_tls_version     = var.tls_release
    cdn_frontdoor_secret_id = local.tls_secret
  }
}

###############################################################################
# Outputs
###############################################################################

output "class" {
  value = var.class
}

output "group" {
  value = var.group
}

output "domain" {
  value = var.domain
}

###############################################################################

output "tls_type" {
  value = var.tls_type
}

output "tls_secret" {
  value = local.tls_secret
}

output "tls_release" {
  value = var.tls_release
}

###############################################################################

output "id" {
  value = azurerm_cdn_frontdoor_custom_domain.object.id
}

output "name" {
  value = azurerm_cdn_frontdoor_custom_domain.object.name
}

output "token" {
  value = azurerm_cdn_frontdoor_custom_domain.object.validation_token
}

output "expires" {
  value = azurerm_cdn_frontdoor_custom_domain.object.expiration_date
}

###############################################################################
