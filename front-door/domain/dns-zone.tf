###############################################################################
# Optional Variables
###############################################################################

variable "dns_zone_id" {
  type        = string
  default     = null
  description = "The ID of the DNS zone to associate this Azure Front Door custom domain with."
}

variable "dns_zone_name" {
  type        = string
  default     = null
  description = "The full name of the DNS zone to associate this Azure Front Door custom domain with. Must be specified if 'dns_zone_lookup' is 'true'."
}

variable "dns_zone_lookup" {
  type        = bool
  default     = false
  description = "Should the ID of the DNS zone be looked up from 'dns_zone_name'."
}

###############################################################################
# Locals
###############################################################################

locals {
  dns_zone_id   = var.dns_zone_lookup ? data.azurerm_dns_zone.object[0].id : var.dns_zone_id
  dns_zone_name = var.dns_zone_lookup ? var.dns_zone_name : null
}

###############################################################################
# Data Sources
###############################################################################

data "azurerm_dns_zone" "object" {
  count               = var.dns_zone_lookup ? 1 : 0
  name                = local.dns_zone_name
  resource_group_name = var.group
}

###############################################################################
# Outputs
###############################################################################

output "dns_zone_id" {
  value = local.dns_zone_id
}

output "dns_zone_name" {
  value = local.dns_zone_name
}

output "dns_zone_lookup" {
  value = var.dns_zone_lookup
}

###############################################################################
