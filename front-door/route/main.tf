###############################################################################
# Required Variables
###############################################################################

variable "class" {
  type        = string
  description = "The identifier of the Azure Front Door route within the Azure Front Door profile. Set to 'null' for no identifier (assume same name as the profile)."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

###############################################################################

variable "origin_ids" {
  type        = set(string)
  description = "The IDs of the Azure Front Door origins that this Azure Front Door route should send requests to."

  validation {
    condition     = length(compact(var.origin_ids)) > 0
    error_message = "At least one Azure Front Door origin ID must be specified."
  }
}

variable "origin_group" {
  type        = string
  description = "The ID of the Azure Front Door origin group to associate this Azure Front Door route with."
}

###############################################################################
# Optional Variables
###############################################################################

variable "enabled" {
  type        = bool
  default     = true
  description = "Should this Azure Front Door route be enabled?"
}

variable "patterns" {
  type        = list(string)
  default     = [ "/*" ]
  description = "The incoming request URL patterns to match for this Azure Front Door route."

  validation {
    condition     = length(compact(var.patterns)) > 0
    error_message = "URL patterns must not be empty."
  }
}

variable "redirect" {
  type        = bool
  default     = true
  description = "Redirect HTTP traffic to HTTPS. Ignored unless both 'Http' and 'Https' are set in 'protocols'."
}

variable "protocols" {
  type        = list(string)
  default     = [ "Http", "Https" ]
  description = "One or more protocols supported by connections incoming to this Azure Front Door route."

  validation {
    condition     = alltrue([ for protocol in var.protocols : contains([ "Http", "Https" ], protocol) ])
    error_message = "Endpoint protocols must contain 'Http', 'Https' or both."
  }

  validation {
    condition     = length(compact(var.protocols)) > 0
    error_message = "Endpoint protocols must contain 'Http', 'Https' or both."
  }
}

variable "rule_sets" {
  type        = list(string)
  default     = null
  description = "The Azure Front Door rule sets to associate with this Azure Front Door route."
}

###############################################################################

variable "cache_enabled" {
  type        = bool
  default     = true
  description = "Should caching be enabled on this Azure Front Door route? If 'false' all other cache settings are ignored."
}

###############################################################################

variable "cache_compression_types" {
  type    = list(string)
  default = [
    "application/eot",
    "application/font",
    "application/font-sfnt",
    "application/javascript",
    "application/json",
    "application/opentype",
    "application/otf",
    "application/pkcs7-mime",
    "application/truetype",
    "application/ttf",
    "application/vnd.ms-fontobject",
    "application/xhtml+xml",
    "application/xml",
    "application/xml+rss",
    "application/x-font-opentype",
    "application/x-font-truetype",
    "application/x-font-ttf",
    "application/x-httpd-cgi",
    "application/x-mpegurl",
    "application/x-opentype",
    "application/x-otf",
    "application/x-perl",
    "application/x-ttf",
    "application/x-javascript",
    "font/eot",
    "font/ttf",
    "font/otf",
    "font/opentype",
    "image/svg+xml",
    "text/css",
    "text/csv",
    "text/html",
    "text/javascript",
    "text/js",
    "text/plain",
    "text/richtext",
    "text/tab-separated-values",
    "text/xml",
    "text/x-script",
    "text/x-component",
    "text/x-java-source"
  ]
  description = "A list of MIME types that should be compressed. Only applies if both 'cache_compression_enabled' and 'cache_enabled' are 'true'."
}

variable "cache_compression_enabled" {
  type        = bool
  default     = true
  description = "Should content be compressed in transit?"
}

###############################################################################

variable "cache_query_string_list" {
  type        = list(string)
  default     = []
  description = "Query strings that the cache should include or ignore. Ignored unles 'cache_query_string_type' is either 'IgnoreSpecifiedQueryStrings' or 'IncludeSpecifiedQueryStrings'."
}

variable "cache_query_string_type" {
  type        = string
  default     = "UseQueryString"
  description = "Defines how query strings will effect caching. Must be either 'IgnoreQueryString', 'IgnoreSpecifiedQueryStrings', 'IncludeSpecifiedQueryStrings' or 'UseQueryString'."

  validation {
    condition     = contains([ "IgnoreQueryString", "IgnoreSpecifiedQueryStrings", "IncludeSpecifiedQueryStrings", "UseQueryString" ], var.cache_query_string_type)
    error_message = "Query string caching type must be either 'IgnoreQueryString', 'IgnoreSpecifiedQueryStrings', 'IncludeSpecifiedQueryStrings' or 'UseQueryString'."
  }
}

###############################################################################

variable "domain_list" {
  type        = list(string)
  default     = null
  description = "A list of Azure Front Door custom domains to associate with this Azure Front Door route."

  validation {
    condition     = try(length(var.domain_list) > 0, true)
    error_message = "List of Azure Front Door custom domains must contain at least one domain (or not be specified at all)."
  }
}

variable "domain_default" {
  type        = bool
  default     = null
  description = "Should the default Azure Front Door domain be linked to this route? Defaults to 'true' if 'domain_list' is not specified, otherwise defaults to 'false'."
}

###############################################################################

variable "origin_path" {
  type        = string
  default     = null
  description = "The path on the origin to retrieve content from."
}

variable "origin_protocol" {
  type        = string
  default     = "HttpsOnly"
  description = "The protocol that will be used to communicate with the origin. Must be 'HttpOnly', 'HttpsOnly' or 'MatchRequest'."
}

###############################################################################
# Locals
###############################################################################

locals {
  name                    = var.class == null ? var.endpoint_profile : format("%s-%s", var.endpoint_profile, var.class)
  redirect                = contains(var.protocols, "Http") && contains(var.protocols, "Https") ? var.redirect : false
  domain_default          = coalesce(var.domain_default, var.domain_list == null ? true : false)
  cache_query_string_list = contains([ "IgnoreSpecifiedQueryStrings", "IncludeSpecifiedQueryStrings" ], var.cache_query_string_type) ? var.cache_query_string_list : null
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_cdn_frontdoor_route" "object" {
  name                            = local.name
  enabled                         = var.enabled
  patterns_to_match               = var.patterns
  forwarding_protocol             = var.origin_protocol
  supported_protocols             = var.protocols
  https_redirect_enabled          = local.redirect
  link_to_default_domain          = local.domain_default
  cdn_frontdoor_origin_ids        = var.origin_ids
  cdn_frontdoor_endpoint_id       = local.endpoint_id
  cdn_frontdoor_origin_path       = var.origin_path
  cdn_frontdoor_rule_set_ids      = var.rule_sets
  cdn_frontdoor_origin_group_id   = var.origin_group
  cdn_frontdoor_custom_domain_ids = var.domain_list

  dynamic "cache" {
    for_each = var.cache_enabled ? [ null ] : []

    content {
      query_strings                 = local.cache_query_string_list
      compression_enabled           = var.cache_compression_enabled
      content_types_to_compress     = var.cache_compression_types
      query_string_caching_behavior = var.cache_query_string_type
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "class" {
  value = var.class
}

output "group" {
  value = var.group
}

###############################################################################

output "origin_ids" {
  value = var.origin_ids
}

output "origin_group" {
  value = var.origin_group
}

###############################################################################

output "enabled" {
  value = var.enabled
}

output "patterns" {
  value = var.patterns
}

output "redirect" {
  value = local.redirect
}

output "protocols" {
  value = var.protocols
}

output "rule_sets" {
  value = var.rule_sets
}

###############################################################################

output "cache_enabled" {
  value = var.cache_enabled
}

###############################################################################

output "cache_compression_types" {
  value = var.cache_compression_types
}

output "cache_compression_enabled" {
  value = var.cache_compression_enabled
}

###############################################################################

output "cache_query_string_list" {
  value = local.cache_query_string_list
}

output "cache_query_string_type" {
  value = var.cache_query_string_type
}

###############################################################################

output "domain_list" {
  value = var.domain_list
}

output "domain_default" {
  value = local.domain_default
}

###############################################################################

output "origin_path" {
  value = var.origin_path
}

output "origin_protocol" {
  value = var.origin_protocol
}

###############################################################################

output "id" {
  value = azurerm_cdn_frontdoor_route.object.id
}

output "name" {
  value = azurerm_cdn_frontdoor_route.object.name
}

###############################################################################
