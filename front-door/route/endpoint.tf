###############################################################################
# Required Variables
###############################################################################

variable "endpoint_name" {
  type        = string
  description = "The full name of the Azure Front Door endpoint to associate this Azure Front Door route with."
}

variable "endpoint_profile" {
  type        = string
  description = "The full name of the Azure Front Door profile that 'endpoint_name' is associated with."
}

###############################################################################
# Optional Variables
###############################################################################

variable "endpoint_id" {
  type        = string
  default     = null
  description = "The ID of the Azure Front Door endpoint to associate this Azure Front Door route with. Must be specified if the endpoint is created in the same Terraform run."
}

variable "endpoint_lookup" {
  type        = bool
  default     = true
  description = "Should the ID of the Azure Front Door endpoint be looked up from 'endpoint_name'. Must be 'true' if 'endpoint_id' is not specified."
}

###############################################################################
# Locals
###############################################################################

locals {
  endpoint_id = var.endpoint_lookup ? data.azurerm_cdn_frontdoor_endpoint.object[0].id : var.endpoint_id
}

###############################################################################
# Data Sources
###############################################################################

data "azurerm_cdn_frontdoor_endpoint" "object" {
  count               = var.endpoint_lookup ? 1 : 0
  name                = var.endpoint_name
  profile_name        = var.endpoint_profile
  resource_group_name = var.group
}

###############################################################################
# Outputs
###############################################################################

output "endpoint_name" {
  value = var.endpoint_name
}

output "endpoint_profile" {
  value = var.endpoint_profile
}

###############################################################################

output "endpoint_id" {
  value = local.endpoint_id
}

output "endpoint_lookup" {
  value = var.endpoint_lookup
}

###############################################################################
