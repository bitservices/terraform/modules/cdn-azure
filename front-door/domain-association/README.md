<!---------------------------------------------------------------------------->

# front-door/domain-association

#### [Azure] [Front Door] custom domain and route association

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cdn/azure//front-door/domain-association`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = "uksouth"
}

module "my_front_door_profile" {
  source  = "gitlab.com/bitservices/cdn/azure//front-door/profile"
  name    = "foobar-frontdoor"
  group   = module.my_resource_group.name
  owner   = var.owner
  company = var.company
}

module "my_front_door_endpoint" {
  source         = "gitlab.com/bitservices/cdn/azure//front-door/endpoint"
  class          = null
  group          = module.my_resource_group.name
  owner          = var.owner
  company        = var.company
  profile_id     = module.my_front_door_profile.id
  profile_name   = module.my_front_door_profile.name
  profile_lookup = false
}

module "my_front_door_origins" {
  source         = "gitlab.com/bitservices/cdn/azure//front-door/origins"
  class          = null
  group          = module.my_resource_group.name
  profile_id     = module.my_front_door_profile.id
  profile_name   = module.my_front_door_profile.name
  profile_lookup = false

  origins = {
    "foo" = {
      endpoint = "origin.foo.bitservices.io"
    }
  }
}

module "my_front_door_domain" {
  source         = "gitlab.com/bitservices/cdn/azure//front-door/domain"
  class          = "foo"
  group          = module.my_resource_group.name
  domain         = "foo.bitservices.io"
  profile_id     = module.my_front_door_profile.id
  profile_name   = module.my_front_door_profile.name
  profile_lookup = false
}

module "my_front_door_route" {
  source           = "gitlab.com/bitservices/cdn/azure//front-door/route"
  class            = null
  group            = module.my_resource_group.name
  origin_ids       = module.my_front_door_origins.origins_ids
  domain_list      = toset([ module.my_front_door_domain.id ])
  endpoint_id      = module.my_front_door_endpoint.id
  origin_group     = module.my_front_door_origins.id
  endpoint_name    = module.my_front_door_endpoint.name
  endpoint_lookup  = false
  endpoint_profile = module.my_front_door_endpoint.profile_name
}

module "my_front_door_domain_association" {
  source = "gitlab.com/bitservices/cdn/azure//front-door/domain-association"
  domain = module.my_front_door_domain.id
  routes = tolist([ module.my_front_door_route.id ])
}
```

<!---------------------------------------------------------------------------->

[Azure]:      https://azure.microsoft.com/
[Front Door]: https://azure.microsoft.com/services/frontdoor/

<!---------------------------------------------------------------------------->
