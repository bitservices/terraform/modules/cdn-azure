###############################################################################
# Required Variables
###############################################################################

variable "domain" {
  type        = string
  description = "The Azure Front Door custom domain ID to associate with one or more Azure Front Door routes."
}

variable "routes" {
  type        = list(string)
  description = "A list of Azure Front Door route IDs to attach to 'domain'."

  validation {
    condition     = length(var.routes) > 0
    error_message = "At least one Azure Front Door route ID should be specified!"
  }
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_cdn_frontdoor_custom_domain_association" "object" {
  cdn_frontdoor_route_ids        = var.routes
  cdn_frontdoor_custom_domain_id = var.domain
}

###############################################################################
# Outputs
###############################################################################

output "domain" {
  value = var.domain
}

output "routes" {
  value = var.routes
}

###############################################################################

output "id" {
  value = azurerm_cdn_frontdoor_custom_domain_association.object.id
}

###############################################################################
