<!---------------------------------------------------------------------------->

# front-door/endpoint

#### [Azure] [Front Door] endpoints

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cdn/azure//front-door/endpoint`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = "uksouth"
}

module "my_front_door_profile" {
  source  = "gitlab.com/bitservices/cdn/azure//front-door/profile"
  name    = "foobar-frontdoor"
  group   = module.my_resource_group.name
  owner   = var.owner
  company = var.company
}

module "my_front_door_endpoint" {
  source         = "gitlab.com/bitservices/cdn/azure//front-door/endpoint"
  class          = null
  group          = module.my_resource_group.name
  owner          = var.owner
  company        = var.company
  profile_id     = module.my_front_door_profile.id
  profile_name   = module.my_front_door_profile.name
  profile_lookup = false
}
```

<!---------------------------------------------------------------------------->

[Azure]:      https://azure.microsoft.com/
[Front Door]: https://azure.microsoft.com/services/frontdoor/

<!---------------------------------------------------------------------------->
