###############################################################################
# Required Variables
###############################################################################

variable "class" {
  type        = string
  description = "The identifier of the Azure Front Door endpoint within the Azure Front Door profile. Set to 'null' for no identifier (assume same name as the profile)."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

###############################################################################
# Optional Variables
###############################################################################

variable "enabled" {
  type        = bool
  default     = true
  description = "Specifies if this Azure Front Door endpoint is enabled."
}

###############################################################################
# Locals
###############################################################################

locals {
  name = var.class == null ? var.profile_name : format("%s-%s", var.profile_name, var.class)
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_cdn_frontdoor_endpoint" "object" {
  name                     = local.name
  enabled                  = var.enabled
  cdn_frontdoor_profile_id = local.profile_id

  tags = {
    "Name"         = local.name
    "Class"        = var.class
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Enabled"      = var.enabled ? "Enabled" : "Disabled"
    "Profile"      = var.profile_name
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "class" {
  value = var.class
}

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

###############################################################################

output "enabled" {
  value = var.enabled
}

###############################################################################

output "id" {
  value = azurerm_cdn_frontdoor_endpoint.object.id
}

output "host" {
  value = azurerm_cdn_frontdoor_endpoint.object.host_name
}

output "name" {
  value = azurerm_cdn_frontdoor_endpoint.object.name
}

###############################################################################
