###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the Azure Front Door profile."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

###############################################################################
# Optional Variables
###############################################################################

variable "sku" {
  type        = string
  default     = "Standard_AzureFrontDoor"
  description = "The Azure Front Door profile SKU. Must be: 'Standard_AzureFrontDoor' or 'Premium_AzureFrontDoor'."
}

variable "timeout" {
  type        = number
  default     = 60
  description = "Specifies the maximum response timeout in seconds."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_cdn_frontdoor_profile" "object" {
  name                     = var.name
  sku_name                 = var.sku
  resource_group_name      = var.group
  response_timeout_seconds = var.timeout

  tags = {
    "SKU"          = var.sku
    "Name"         = var.name
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Timeout"      = format("%d Seconds", var.timeout)
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

###############################################################################

output "sku" {
  value = var.sku
}

output "timeout" {
  value = var.timeout
}

###############################################################################

output "id" {
  value = azurerm_cdn_frontdoor_profile.object.id
}

output "guid" {
  value = azurerm_cdn_frontdoor_profile.object.resource_guid
}

output "name" {
  value = azurerm_cdn_frontdoor_profile.object.name
}

###############################################################################
