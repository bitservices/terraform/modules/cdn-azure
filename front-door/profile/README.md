<!---------------------------------------------------------------------------->

# front-door/profile

#### [Azure] [Front Door] profiles

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cdn/azure//front-door/profile`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = "uksouth"
}

module "my_front_door_profile" {
  source  = "gitlab.com/bitservices/cdn/azure//front-door/profile"
  name    = "foobar-frontdoor"
  group   = module.my_resource_group.name
  owner   = var.owner
  company = var.company
}
```

<!---------------------------------------------------------------------------->

[Azure]:      https://azure.microsoft.com/
[Front Door]: https://azure.microsoft.com/services/frontdoor/

<!---------------------------------------------------------------------------->
