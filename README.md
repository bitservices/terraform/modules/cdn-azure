<!---------------------------------------------------------------------------->

# cdn (azure)

<!---------------------------------------------------------------------------->

## Description

Manage [Azure] [Front Door] and other [Content Delivery Network] (CDN)
components.

<!---------------------------------------------------------------------------->

## Modules

* [cdn/profile](cdn/profile/README.md) - [Azure] classic [Content Delivery Network] (CDN) profiles.
* [cdn/endpoint](cdn/endpoint/README.md) - [Azure] classic [Content Delivery Network] (CDN) endpoints.
* [front-door/domain](front-door/domain/README.md) - [Azure] [Front Door] custom domains.
* [front-door/domain-association](front-door/domain-association/README.md) - [Azure] [Front Door] custom domain and route association.
* [front-door/endpoint](front-door/endpoint/README.md) - [Azure] [Front Door] endpoints.
* [front-door/origins](front-door/origins/README.md) - [Azure] [Front Door] origin groups and their respective origins.
* [front-door/profile](front-door/profile/README.md) - [Azure] [Front Door] profiles.
* [front-door/route](front-door/route/README.md) - [Azure] [Front Door] routes.


<!---------------------------------------------------------------------------->

[Azure]:                    https://azure.microsoft.com/
[Front Door]:               https://azure.microsoft.com/services/frontdoor/
[Content Delivery Network]: https://azure.microsoft.com/services/cdn/

<!---------------------------------------------------------------------------->
