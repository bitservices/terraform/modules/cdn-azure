###############################################################################
# Required Variables
###############################################################################

variable "class" {
  type        = string
  description = "The identifier of the Azure CDN endpoint within the Azure CDN profile. Set to 'null' for no identifier (assume same name as the profile)."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "profile" {
  type        = string
  description = "The full name of the Azure CDN profile to associate this Azure CDN endpoint with."
}

variable "location" {
  type        = string
  description = "Datacentre location for this Azure CDN endpoint."
}

###############################################################################

variable "origins" {
  type = list(object({
    host       = string
    name       = string
    http_port  = optional(number)
    https_port = optional(number)
  }))
  description = "The set of origins of the Azure CDN endpoint. When multiple origins exist, the first origin will be used as primary and rest will be used as failover options."

  validation {
    condition     = length(var.origins) > 0
    error_message = "At least one valid origin must be specified for the Azure CDN endpoint."
  }
}

###############################################################################
# Optional Variables
###############################################################################

variable "http_enabled" {
  type        = bool
  default     = false
  description = "Should HTTP be enabled?"
}

variable "http_default_port" {
  type        = number
  default     = 80
  description = "Default port HTTP port for origins if not specified."
}

###############################################################################

variable "https_enabled" {
  type        = bool
  default     = true
  description = "Should HTTPS be enabled? Always 'true' if 'http_enabled' is 'false'."
}

variable "https_default_port" {
  type        = number
  default     = 443
  description = "Default port HTTPS port for origins if not specified."
}

###############################################################################
# Locals
###############################################################################

locals {
  name          = var.class == null ? var.profile : format("%s-%s", var.profile, var.class)
  https_enabled = var.http_enabled ? var.https_enabled : true

  origins = defaults(var.origins, {
    "http_port"  = var.http_default_port
    "https_port" = var.https_default_port
  })
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_cdn_endpoint" "object" {
  name                = local.name
  location            = var.location
  profile_name        = var.profile
  is_http_allowed     = var.http_enabled
  is_https_allowed    = local.https_enabled
  resource_group_name = var.group

  tags = {
    "Name"         = local.name
    "Class"        = var.class
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Profile"      = var.profile
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }

  dynamic "origin" {
    for_each = local.origins

    content {
      name       = origin.value.name
      host_name  = origin.value.host
      http_port  = origin.value.http_port
      https_port = origin.value.https_port
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "class" {
  value = var.class
}

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "profile" {
  value = var.profile
}

output "location" {
  value = var.location
}

###############################################################################

output "origins" {
  value = local.origins
}

###############################################################################

output "http_enabled" {
  value = var.http_enabled
}

output "http_default_port" {
  value = var.http_default_port
}

###############################################################################

output "https_enabled" {
  value = var.https_enabled
}

output "https_default_port" {
  value = var.https_default_port
}

###############################################################################

output "id" {
  value = azurerm_cdn_endpoint.object.id
}

#output "fqdn" {
#  value = azurerm_cdn_endpoint.object.fqdn
#}

output "name" {
  value = azurerm_cdn_endpoint.object.name
}

###############################################################################
