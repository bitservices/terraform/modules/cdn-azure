<!---------------------------------------------------------------------------->

# cdn/endpoint

#### [Azure] classic [Content Delivery Network] (CDN) endpoints

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/cdn/azure//cdn/endpoint`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_cdn_profile" {
  source   = "gitlab.com/bitservices/cdn/azure//cdn/profile"
  name     = "foobar-cdn"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_cdn_endpoint" {
  source   = "gitlab.com/bitservices/cdn/azure//cdn/endpoint"
  class    = null
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  profile  = module.my_cdn_profile.name
  location = var.location

  origins = [{
    "host" = "origin.foo.bitservices.io"
    "name" = "default"
  }]
}
```

<!---------------------------------------------------------------------------->

[Azure]:                    https://azure.microsoft.com/
[Content Delivery Network]: https://azure.microsoft.com/services/cdn/

<!---------------------------------------------------------------------------->
