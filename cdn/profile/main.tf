###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The full name of the Azure CDN profile."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this resource."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this Azure CDN profile."
}

###############################################################################
# Optional Variables
###############################################################################

variable "sku" {
  type        = string
  default     = "Standard_Microsoft"
  description = "The Azure CDN profile SKU. Must be: 'Standard_Akamai', 'Standard_ChinaCdn', 'Standard_Microsoft', 'Standard_Verizon' or 'Premium_Verizon'."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_cdn_profile" "object" {
  sku                 = var.sku
  name                = var.name
  location            = var.location
  resource_group_name = var.group

  tags = {
    "SKU"          = var.sku
    "Name"         = var.name
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "location" {
  value = var.location
}

###############################################################################

output "sku" {
  value = var.sku
}

###############################################################################

output "id" {
  value = azurerm_cdn_profile.object.id
}

output "name" {
  value = azurerm_cdn_profile.object.name
}

###############################################################################
